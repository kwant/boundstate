This package implements an algorithm for computing bound states of infinite
tight-binding systems that are made up of a finite scattering region connected
to semi-infinite leads.  The details of the algorithms are explained in an
`accompanying article <dx.doi.org/10.21468/SciPostPhys.4.5.026>`_.

This package is free software under the `BSD License <LICENSE.rst>`_.  If you
use it for work that leads to a scientific publication, please cite the above
article.
